﻿using Comics.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaptainAmazingConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<Comic> comics = Comic.BuildCatalog();
            Dictionary<int, decimal> values = Comic.GetPrices();

            var mostExpensive =
                from comic in comics
                where values[comic.Issue] > 500
                orderby values[comic.Issue] descending
                select comic;

            foreach (Comic comic in mostExpensive)
            {
                Console.WriteLine("{0} is worth {1:c}", comic.Name, values[comic.Issue]);
            }

            Console.ReadKey();
        }
    }
}
