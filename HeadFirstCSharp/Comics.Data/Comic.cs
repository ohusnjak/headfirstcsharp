﻿namespace Comics.Data
{
    public class Comic
    {
        public string Name { get; set; }
        public int Issue { get; set; }
    }
}
