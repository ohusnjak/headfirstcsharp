﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comics.Data
{
    public enum PriceRange
    {
        Cheap,
        Midrange,
        Expensive,
    }
}
