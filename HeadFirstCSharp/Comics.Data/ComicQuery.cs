﻿using System.Windows.Media.Imaging;

namespace Comics.Data
{
    class ComicQuery
    {
        public string Title { get; private set; }
        public string SubTitle { get; private set; }
        public string Description { get; private set; }
        public BitmapImage Image { get; private set; }

        public ComicQuery(string title, string subtitle, string description, BitmapImage image)
        {
            Title = title;
            SubTitle = subtitle;
            Description = description;
            Image = image;
        }
    }
}
